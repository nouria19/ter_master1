package com.interim;

import com.interim.Entity.User;
import com.interim.service.UserService;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.springframework.web.bind.annotation.RequestBody;
import proxy.EmployerProxy;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;



@Path("/auth")
@ApplicationScoped
public class AuthResource {

    @RestClient
    EmployerProxy employerProxy;
    @Inject
    UserService userService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(UserCredentials credentials) {
      //  User user = userService.authenticate(credentials.getUsername(), credentials.getPassword());
       // if (user != null) {
            String token = userService.generateJWT("user.getUsername()"," user.getRole()");
            return Response.ok().entity(new TokenResponse(token)).build();
       /* } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }*/
    }



    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response logind() {
        //User user = userService.authenticate(credentials.getUsername(), credentials.getPassword());
        //if (user != null) {
        String token = userService.generateJWT("user.getUsername()"," user.getRole()");
        //     return Response.ok().entity(new TokenResponse(token)).build();
        //  } else {
        employerProxy.get();
        return Response.ok(token).build();
        //}
    }
    @GET
    @RolesAllowed({"employer", "anonyme"})
        @Path("/companies")
        @Produces(MediaType.TEXT_PLAIN)
        public Response getAllCompanies() {
            //User user = userService.authenticate(credentials.getUsername(), credentials.getPassword());
            //if (user != null) {
            String token = userService.generateJWT("user.getUsername()"," user.getRole()");
            //     return Response.ok().entity(new TokenResponse(token)).build();
            //  } else {
            return employerProxy.get();
        //return Response.ok(token).build();
        //}
    }

    @POST
    @Transactional
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser( User user) {
        user = userService.create(user);
        return Response.ok(user).build();
    }

//    @GET
//    @Path("users")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response findAllUsers() {
//        List<User> users = userService.findAll();
//        if (users == null) {
//            return Response.status(Response.Status.NOT_FOUND).build();
//        }
//        return Response.ok(users).build();
//    }
//

    static class UserCredentials {
        public String username;
        public String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    static class TokenResponse {
        public String token;
        public TokenResponse(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}