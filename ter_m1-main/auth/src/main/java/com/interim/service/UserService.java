package com.interim.service;


import com.interim.Entity.User;
import io.smallrye.jwt.build.Jwt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class UserService {

    @Inject
    EntityManager em;

    @Transactional
    public User authenticate(String username, String password) {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE u.username = :username AND u.password = :password", User.class);
        query.setParameter("username", username);
        query.setParameter("password", password);
        try {
            User user = query.getSingleResult();
            return user;
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Transactional
    public User create(User user) {
        em.persist(user);
        return user;
    }

    public String generateJWT(String username, String role) {
        Set<String> roles  = new HashSet<>(Arrays.asList("admin", "anonyme"));

        return Jwt.issuer("auth")
                .subject("auth")
                .groups(roles)
                .expiresAt(System.currentTimeMillis() + 3600)
                .sign();

    }

    public List<User> getAllUsers() {
        return User.listAll();
    }

    public User getUserById(Long id) {
        return User.findById(id);
    }

    @Transactional
    public void deleteUser(Long id) {
        User.deleteById(id);
    }


}