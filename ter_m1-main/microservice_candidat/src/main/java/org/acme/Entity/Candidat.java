package org.acme.Entity;
import javax.persistence.*;

import org.bson.types.ObjectId;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;

import java.time.LocalDate;


@MongoEntity(collection="candidat")
public class Candidat extends PanacheMongoEntity { //une candidature généralement est lié à une offre pour l'instant je le laisse comme ça

    @Id
    private ObjectId id;

    private String name;

    private String firstname;

    private LocalDate dateOfBirth;

    private String nationality;



    public Candidat() {}

    public Candidat(String name, String firstname, LocalDate dateOfBirth, String nationality) {
        this.name = name;
        this.firstname = firstname;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

}
