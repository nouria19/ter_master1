package org.acme.Ressources;
import io.quarkus.mongodb.panache.PanacheMongoRepositoryBase;

import java.time.LocalDate;

import javax.enterprise.context.ApplicationScoped;

import org.acme.Entity.Candidat;
import org.bson.types.ObjectId;

@ApplicationScoped
public class CandidatRepository implements PanacheMongoRepositoryBase<Candidat, ObjectId> {
    
    /* chercher une candidat dans la base de données en utilisant à la fois son identifiant */

    public Candidat findByIdAndIdOffre(Long idCandidat) {
        return find("id = ?1", idCandidat).firstResult();
    }


    public Candidat findByNomPrenomNationalite(String name, String firstname, String nationality, LocalDate dateOfBirth) {
        return find("name = ?1 AND firstname = ?2 AND nationality = ?3 AND dateOfBirth = ?4", name, firstname, nationality, dateOfBirth).firstResult();
    }
    
}
