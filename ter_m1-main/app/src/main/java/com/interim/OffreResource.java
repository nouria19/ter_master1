package com.interim;

import com.interim.entity.Offre;
import com.interim.service.OffreService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.List;

@Path("/offres")

public class OffreResource {

    @Inject
    OffreService offreService;

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOffre(Offre offre) {
        offre = offreService.create(offre);
        return Response.ok(offre).build();
    }

    @DELETE
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response deleteOffre(@PathParam("id") Long id) {
        offreService.delete(id);
        return Response.ok().build();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findOffreAll() {
        List<Offre> offres = offreService.findAll();
        if (offres == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(offres).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findOffreById(@PathParam("id") Long id) {
        Offre offre = offreService.findById(id);
        if (offre == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(offre).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOffre(@PathParam("id") Long id, Offre offreToUpdate) {
        offreService.update(id, offreToUpdate);
        return Response.ok().build();
    }


    @GET
    @Path("filter")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Offre> filterOffres(@QueryParam("jobTarget") String jobTarget,
                                    @QueryParam("company") String company,
                                    @QueryParam("location") String location,
                                    @QueryParam("startDate") LocalDate startDate,
                                    @QueryParam("endDate")  LocalDate endDate) {
        return offreService.getOffres(jobTarget, company, location, startDate, endDate);
    }




}
