package com.interim.service;

import com.interim.entity.Candidature;
import com.interim.entity.Offre;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@ApplicationScoped
public class OffreService {

    @Inject
    EntityManager entityManager;

    public List<Offre> findAll() {
        return entityManager.createQuery("SELECT o FROM Offre o", Offre.class).getResultList();
    }

    public Offre findById(Long id) {
        return entityManager.find(Offre.class, id);
    }

    public Offre create(Offre offre) {
        entityManager.persist(offre);
        return offre;
    }

    public void update(Long id, Offre offreToUpdate) {
        Offre existingOffre = entityManager.find(Offre.class, id);
        existingOffre.setTitle(offreToUpdate.getTitle());
        existingOffre.setDescription(offreToUpdate.getDescription());
        existingOffre.setCompany(offreToUpdate.getCompany());
        existingOffre.setJobTarget(offreToUpdate.getJobTarget());
        existingOffre.setLocation(offreToUpdate.getLocation());
        existingOffre.setStartDate(offreToUpdate.getStartDate());
        existingOffre.setEndDate(offreToUpdate.getEndDate());
        entityManager.merge(existingOffre);
    }

    public void delete(Long id) {
        Offre offre = entityManager.find(Offre.class, id);
        entityManager.remove(offre);
    }

    public List<Offre> findByCriteres(String jobTarget, String location, LocalDate startDate, LocalDate endDate) {
        String queryString = "SELECT o FROM Offre o WHERE o.jobTarget = :jobTarget AND o.location = :location AND o.startDate >= :startDate AND o.endDate <= :endDate";
        TypedQuery<Offre> query = entityManager.createQuery(queryString, Offre.class);
        query.setParameter("jobTarget", jobTarget);
        query.setParameter("location", location);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        return query.getResultList();
    }

    public List<Offre> getOffres(String jobTarget, String company, String location, LocalDate startDate, LocalDate endDate) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Offre> cq = cb.createQuery(Offre.class);
        Root<Offre> root = cq.from(Offre.class);

        List<Predicate> predicates = new ArrayList<>();

        if (jobTarget != null && !jobTarget.isEmpty()) {
            System.out.println("title");
            predicates.add(cb.like(cb.lower(root.get("jobTarget")), "%" + jobTarget.toLowerCase() + "%"));
        }

        if (company != null && !company.isEmpty()) {
            System.out.println("company");
            predicates.add(cb.like(cb.lower(root.get("company")), "%" + company.toLowerCase() + "%"));
        }

        if (location != null && !location.isEmpty()) {
            System.out.println("location");
            predicates.add(cb.like(cb.lower(root.get("location")), "%" + location.toLowerCase() + "%"));
        }

        if (startDate != null) {
            System.out.println("startDate");
            predicates.add(cb.greaterThanOrEqualTo(root.get("startDate"), startDate));
        }

        if (endDate != null) {
            System.out.println("endDate");
            predicates.add(cb.lessThanOrEqualTo(root.get("endDate"), endDate));
        }

        cq.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Offre> query = entityManager.createQuery(cq);
        System.out.println("end");
        return query.getResultList();
    }

    public void addCandidature(Long offreId, Candidature candidature) {
        Offre offre = entityManager.find(Offre.class, offreId);
        //candidature.setJobOffer(offre);
        entityManager.persist(candidature);
        offre.getCandidatures().add(candidature);
        entityManager.merge(offre);
    }

    public List<Candidature> getCandidatures(Long offreId) {
        Offre offre = entityManager.find(Offre.class, offreId);
        return offre.getCandidatures();
    }

}