package com.interim.entity;



import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;



import javax.persistence.Id;

import org.bson.types.ObjectId;

@MongoEntity(collection="InterimAgency")
public class InterimAgency extends  PanacheMongoEntity {
    @Id
    private ObjectId id;

    private String name;
    private String departementName;
    private String subDepartementName;
    private String companyNationalId;

    private String contact1Name;
    private String contact1Email;
    private String contact2Name;
    private String contact2Email;

    private String address;
    private String city;
    private String country;
    private String code;

    public InterimAgency() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartementName() {
        return departementName;
    }

    public void setDepartementName(String departementName) {
        this.departementName = departementName;
    }

    public String getSubDepartementName() {
        return subDepartementName;
    }

    public void setSubDepartementName(String subDepartementName) {
        this.subDepartementName = subDepartementName;
    }

    public String getCompanyNationalId() {
        return companyNationalId;
    }

    public void setCompanyNationalId(String companyNationalId) {
        this.companyNationalId = companyNationalId;
    }

    public String getContact1Name() {
        return contact1Name;
    }

    public void setContact1Name(String contact1Name) {
        this.contact1Name = contact1Name;
    }

    public String getContact1Email() {
        return contact1Email;
    }

    public void setContact1Email(String contact1Email) {
        this.contact1Email = contact1Email;
    }

    public String getContact2Name() {
        return contact2Name;
    }

    public void setContact2Name(String contact2Name) {
        this.contact2Name = contact2Name;
    }

    public String getContact2Email() {
        return contact2Email;
    }

    public void setContact2Email(String contact2Email) {
        this.contact2Email = contact2Email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
