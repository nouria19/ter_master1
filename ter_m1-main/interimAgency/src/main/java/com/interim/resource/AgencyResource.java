package com.interim.resource;



import com.interim.entity.InterimAgency;
import com.interim.service.AgencyService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



import java.util.List;

@Path("/InterimAgency")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AgencyResource {

    @Inject
    AgencyService agencyService;

    @GET
    public Response getAllAgencies() {
        List<InterimAgency> companies = agencyService.getAllAgencies();
        return Response.ok(companies).build();
    }

    @GET
    @Path("/{id}")
    public Response getAgencyById(@PathParam("id") String id) {
        InterimAgency agency =agencyService.getAgencyById(id);
        if (agency != null) {
            return Response.ok(agency).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    public Response createAgency(InterimAgency agency) {
        InterimAgency newAgency = agencyService.createAgency(agency);
        return Response.ok(newAgency).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateAgency(@PathParam("id") String id, InterimAgency agency) {
       agencyService.updateAgency(id, agency);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAgency(@PathParam("id") String id) {
        agencyService.deleteAgency(id);
        return Response.ok().build();
    }

}