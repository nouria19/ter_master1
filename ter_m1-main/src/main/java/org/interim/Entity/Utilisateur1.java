package org.interim.Entity;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;


import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;




@Entity
@Table(name = "utilisateur")
public class Utilisateur1 extends  PanacheEntity{
  
  
  @Column(name = "nom_utilisateur", nullable = false)
  private String username;
  @Column(name = "mot_de_passe", nullable = false)
  private String password;
  @Column(name = "role",nullable = true )
  public String role;
  public String nom;
  public String prenom;
  public String nationalite;
  public LocalDate date_naissance;

  
  
  // Getters et setters
  public Utilisateur1(){

  }
  public Utilisateur1(String user,String motPasse,String role){
   
    this.password=motPasse;
    this.username=user;
    this.role=role;
  }
  
  public String getNom(){
    return username;
  }
  public void setNom(String nom){
    this.username=nom;
  }
  public String getMont(){
    return password;
  }
  public void setMot(String mot){
    this.password=mot;
  }
  public String getRole(){
    return role;
  }
  public void setRole(String role){
    this.role=role;
  }
  public String getNomUtilisateur(){
    return nom;
  }
  public void setNomUtilisateur(String nomUtilisateur){
    this.nom=nomUtilisateur;
  }
  public String getPrenom(){
    return prenom; 
  }
  public void setPrenom(String prenom){
    this.prenom=prenom;
  }
  public String getNationalite(){
    return nationalite;
  }
  public void setNationalite(String nationalite){
    this.nationalite=nationalite;
  }
  public LocalDate getDate(){
    return date_naissance;
  }
  public void setDate(LocalDate date){
    this.date_naissance=date;
  }
}
