package org.interim.Ressource;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.interim.Entity.Utilisateur1;
import org.interim.proxy.Candidat;
import org.interim.proxy.CandidatServiceClient;
import org.interim.proxy.UsersServiceClient;

import javax.ws.rs.core.MediaType;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {
  @PersistenceContext
  EntityManager entityManager;
  @RestClient
  
    UsersServiceClient usersServiceClient;

    @Inject
    @RestClient
    CandidatServiceClient candidatService;


  @POST
  @Transactional
  
  public Response createUser(Utilisateur1 user) {
    
   
    if (user.getMont() == null || user.getMont().isEmpty()) {
        throw new IllegalArgumentException("Password cannot be empty");
      }
      Utilisateur1 utilisateurAEnregistrer = new Utilisateur1();
      utilisateurAEnregistrer.setNom(user.getNom());
      utilisateurAEnregistrer.setMot(user.getMont());
      utilisateurAEnregistrer.setRole(user.getRole());

      entityManager.persist(utilisateurAEnregistrer);

    /*  créer un objet Utilisateur à envoyer au microservice distant
    User utilisateur = new User();
    utilisateur.setNom(user.getNom());
    utilisateur.setPrenom(user.getPrenom());

    // envoyer une requête au microservice distant pour créer un utilisateur
   // usersServiceClient.createUser(utilisateur);*/

   Candidat candidat = new Candidat();
   candidat.setNom(user.getNom());
   candidat.setPrenom(user.getPrenom());
   candidat.setDateNaissance(user.getDate());
   candidat.setNationalite(user.getNationalite());
   
   candidatService.createCandidat(candidat);

    Response.created(UriBuilder.fromResource(UserResource.class).path(user.getNom()).build())
            .build();

      return Response.created(UriBuilder.fromResource(UserResource.class).path(utilisateurAEnregistrer.getNom()).build())
              .build();
  }
}

