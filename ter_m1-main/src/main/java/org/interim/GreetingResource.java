package org.interim;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.interim.Entity.Utilisateur1;

@Path("/hello")
public class GreetingResource {

   
    @Inject
    DataSource dataSource;
    @GET
    public List<Utilisateur1> getAllOffres() throws SQLException {
        List<Utilisateur1> offres = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT * FROM utilisateur");
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                Utilisateur1 offre = new Utilisateur1();
               
                offre.setNom(rs.getString("nom_utilisateur"));
                offre.setMot(rs.getString("mot_de_passe"));
                // TODO: ajouter d'autres propriétés de l'offre
                offres.add(offre);
            };
        }
        return offres;
    }

    

}