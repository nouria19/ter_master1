package gestion.interim.Entity;

import javax.persistence.Id;

import org.bson.types.ObjectId;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;

@MongoEntity(collection="Subscribe")
public class Abonnement extends PanacheMongoEntity {
    @Id
    private ObjectId id;

    public String type;
    public double price;
    public Abonnement(){}
    public Abonnement(String type,double price,ObjectId id){
        this.price=price;
        this.type=type;
        this.id=id;
    }
    public String getType(){
        return type;
    }
    public double getPrice(){
        return price;
    }
    public void setType(String type){
        this.type=type;
    }
    public void setPrice(double price){
        this.price=price;
    }
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

}
