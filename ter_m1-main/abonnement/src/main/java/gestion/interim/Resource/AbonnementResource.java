package gestion.interim.Resource;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import gestion.interim.Entity.Abonnement;
import gestion.interim.Service.AbonnementService;

import java.net.URI;
import java.util.List;

@Path("/subscription")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class AbonnementResource {
    public AbonnementResource(){}


    @Inject
    AbonnementService abonnementRepository;
    @Transactional
    @GET
    @Produces(MediaType.APPLICATION_JSON)
  
    public List<Abonnement> list() {
        return abonnementRepository.listAll();
    }
    @Transactional
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
   
    public Abonnement  get(@PathParam("id") String id) {
        return abonnementRepository.findById(new ObjectId(id));
    }
    @Transactional
    @POST
   
    @Produces(MediaType.APPLICATION_JSON)
   

    public Response create(Abonnement abonnement) {
      

     
   abonnementRepository.createAbonnement(abonnement);

    return Response.created(URI.create("/Subscribe/" + abonnement.getId())).build();
}


@PUT
@Path("/{id}")
@Transactional
public Response updateAbonnement(@PathParam("id") String id, Abonnement abonnement) {
 abonnementRepository.updateAbonnement(id, abonnement);

    return Response.ok(abonnement).build();
}

    @Transactional
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")

    public  Response delete(@PathParam("id") String id) {
       
            abonnementRepository.deleteAbonnement(id);

        
        return Response.noContent().build();
    }
}
