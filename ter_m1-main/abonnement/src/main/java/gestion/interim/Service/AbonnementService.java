package gestion.interim.Service;



















import javax.enterprise.context.ApplicationScoped;

import org.bson.types.ObjectId;

import gestion.interim.Entity.Abonnement;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.mongodb.panache.PanacheMongoRepositoryBase;




@ApplicationScoped
public class AbonnementService implements PanacheMongoRepositoryBase<Abonnement,ObjectId> {
    public Abonnement createAbonnement(Abonnement abonnement) {
        abonnement.persist();
        return abonnement;
    }

    public Abonnement updateAbonnement(String id, Abonnement abonnementToUpdate) {
        Abonnement abonnement = findById(new ObjectId(id));
        if (abonnement == null) {
            return null;
        }
        if (abonnementToUpdate.getType() != null) {
            abonnement.setType(abonnementToUpdate.getType());
        }
        if (abonnementToUpdate.getPrice() != 0) {
            abonnement.setPrice(abonnementToUpdate.getPrice());
        }
        abonnement.persistOrUpdate();
        return abonnement;
    }
    
    public void deleteAbonnement(String id) {
        Abonnement abonnement = findById(new ObjectId(id));
        if (abonnement != null) {
            abonnement.delete();
           
        }
    }

    
    

}