package com.interim.service;


import com.interim.entity.Company;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CompanyService {

    @Transactional
    public Company createCompany(Company company) {
        company.persist();
        return company;
    }

    public List<Company> getAllCompanies() {
        return Company.listAll();
    }

    public Company getCompanyById(Long id) {
        return Company.findById(id);
    }

    /*
     private String name;
    private String departement_name;
    private String subDepartementName;
    private String companyNationalId;

    private String contact1Name;
    private String contact1Email;
    private String contact2Name;
    private String contact2Email;

    private String address;
    private String city;
    private String country;
    private String code;
     */
    @Transactional
    public void updateCompany(Long id, Company company) {
        Company entity = Company.findById(id);
        entity.setName(company.getName());
        entity.setDepartementName(company.getDepartementName());
        entity.setSubDepartementName(company.getSubDepartementName());
        entity.setCompanyNationalId(company.getCompanyNationalId());
        entity.setContact1Name(company.getContact1Name());
        entity.setContact1Email(company.getContact1Email());
        entity.setContact2Name(company.getContact2Name());
        entity.setContact2Email(company.getContact2Email());
        entity.setAddress(company.getAddress());
        entity.setCity(company.getCity());
        entity.setCode(company.getCode());
    }

    @Transactional
    public void deleteCompany(Long id) {
        Company.deleteById(id);
    }

}